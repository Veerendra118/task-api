﻿using NBench;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Entities;

namespace TaskManager.PerformanceTests
{
    public class MemoryTest
    {
        private const string appUrl = "http://localhost/TaskManagerService/api/";

        private static HttpClient client = new HttpClient() { BaseAddress = new Uri(MemoryTest.appUrl) };
        private const int NumberOfReads = 500;
        private const int TaskListSize = 24000;
        private const int MaxExpectedMemory = NumberOfReads * TaskListSize;

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Measurement)]
        [MemoryMeasurement(MemoryMetric.TotalBytesAllocated)]
        public void AddMemoryMeasurement()
        {
            var data = new Dictionary<int, TaskEntity[]>();

            Populate(data, NumberOfReads);
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, MaxExpectedMemory)]
        public void AddMemory_FailingTest()
        {
            var data = new Dictionary<int, TaskEntity[]>();

            Populate(data, NumberOfReads);
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, MaxExpectedMemory)]
        public void AddMemory_PassingTest()
        {
            var dictionary = new Dictionary<int, TaskEntity[]>(NumberOfReads);

            Populate(dictionary, NumberOfReads);
        }

        public void Populate(Dictionary<int, TaskEntity[]> list, int n)
        {
            for (var i = 0; i < n; i++)
            {
                    var responseTask = client.GetAsync("tasks");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<TaskEntity[]>();
                        readTask.Wait();
                        list.Add(i, readTask.Result);
                    }
            }
        }
    }
}
