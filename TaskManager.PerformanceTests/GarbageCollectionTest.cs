﻿using NBench;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Entities;

namespace TaskManager.PerformanceTests
{
    public class GarbageCollectionTests
    {

        private const string appUrl = "http://localhost/TaskManagerService/api/";

        private static HttpClient client = new HttpClient() { BaseAddress= new Uri(GarbageCollectionTests.appUrl)};

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Measurement)]
        [GcMeasurement(GcMetric.TotalCollections, GcGeneration.AllGc)]
        public void MeasureGarbageCollections()
        {
            RunTest();
        }

        [PerfBenchmark(RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [GcThroughputAssertion(GcMetric.TotalCollections, GcGeneration.Gen0, MustBe.LessThan, 300)]
        [GcThroughputAssertion(GcMetric.TotalCollections, GcGeneration.Gen1, MustBe.LessThan, 150)]
        [GcThroughputAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThan, 20)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThan, 50)]
        public void TestGarbageCollections()
        {
            RunTest();
        }

        private readonly List<TaskEntity[]> dataCache = new List<TaskEntity[]>();

        private void RunTest()
        {
            for (var i = 0; i < 50; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    var responseTask = client.GetAsync("tasks");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<TaskEntity[]>();
                        readTask.Wait();
                        dataCache.Add(readTask.Result);
                    }
                }
                dataCache.Clear();
            }
        }
    }
}
