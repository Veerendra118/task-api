﻿namespace TaskManager.Entities
{
    public enum StatusCodes
    {
        Success=0,
        Error=1,
        UniqueValidation=2,
        ConcurrancyError=3
    }
}
