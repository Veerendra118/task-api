﻿using System;

namespace TaskManager.Entities
{
    [Serializable]
    public class TaskEntity
    {
        public int TaskId { get; set; }
        public int ParentTaskId { get; set; }
        public string TaskName { get; set; }
        public string ParentTaskName { get; set; }
        public byte Priority { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        /// <summary>
        /// This is used to know whether a task is ended or not . It has two values OPEN / CLOSED
        /// </summary>
        public bool IsTaskEnded
        {
            get
            {
                return (Status == "OPEN" ? false : true);
            }
        }
    }
}
