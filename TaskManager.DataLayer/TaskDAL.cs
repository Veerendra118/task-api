﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using TaskManager.Entities;

namespace TaskManager.DataLayer
{
   public class TaskDAL
    {
        private TaskConnection _dbConnection = new TaskConnection();
       

        public List<TaskEntity> GetTasks()
        {
            List<TaskEntity> tasks = new List<TaskEntity>() ;

            var dbtasks = _dbConnection.GetAllTasks();

            foreach (var item in dbtasks)
            {
                tasks.Add(new TaskEntity()
                {
                    TaskId = item.TaskId
                                              ,
                    ParentTaskId = item.ParentTaskId
                                              ,
                    ParentTaskName = item.ParentTaskName
                                            ,
                    TaskName = item.TaskName
                                                ,
                    Priority = item.Priority
                                               ,
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    Status = item.Status
                });
            }
                return tasks;
        }


       

        public TaskEntity GetTaskById(int taskId)
        {
            TaskEntity t = null;
            try
            {
                 var result = _dbConnection.GetTaskDetailsByTaskId(taskId).First();

                if (result != null && result.TaskId > 0)
                {
                   t= new TaskEntity()
                    {
                        TaskId = t.TaskId,
                        TaskName = t.TaskName,
                        ParentTaskId = t.ParentTaskId,
                        ParentTaskName = t.ParentTaskName,
                        Priority = t.Priority,
                        StartDate = t.StartDate,
                        EndDate = t.EndDate,
                        Status = t.Status
                    };
                }
            }
            catch (Exception)
            {
                t = null;
            }
            return t;
        }

        public StatusCodes InsertTask(ref TaskEntity task)
        {
            StatusCodes code = StatusCodes.UniqueValidation;

            try
            {
                var dbTask = new Task() { TaskId = task.TaskId
                                            , TaskName = task.TaskName
                                            , ParentTaskId = task.ParentTaskId
                                            , Priority = task.Priority
                                            , StartDate = task.StartDate
                                            , EndDate = task.EndDate
                                            , Status = task.Status };

                if (IsTaskNameUnique(task.TaskId, task.TaskName))
                {

                    _dbConnection.Tasks.Add(dbTask);
                    _dbConnection.SaveChanges();
                    task.TaskId = dbTask.TaskId;
                    code = StatusCodes.Success;
                }
                else
                {
                    code = StatusCodes.UniqueValidation;
                }
            }
            catch (Exception ex)
            { code = StatusCodes.Error; }

            return code;
        }

        public StatusCodes UpdateTask(TaskEntity task)
        {
            StatusCodes code = StatusCodes.UniqueValidation;
            try
            {
                if (TaskExists(task.TaskId))
                {
                    if (IsTaskNameUnique(task.TaskId, task.TaskName))
                    {
                        var dbTask = _dbConnection.Tasks.Find(task.TaskId);
                        dbTask.TaskName = task.TaskName;
                        dbTask.ParentTaskId = task.ParentTaskId;
                        dbTask.Priority = task.Priority;
                        dbTask.StartDate = task.StartDate;
                        dbTask.EndDate = task.EndDate;
                        dbTask.Status = task.Status;

                        _dbConnection.Entry(dbTask).State = EntityState.Modified;
                        _dbConnection.SaveChanges();
                        code = StatusCodes.Success;
                    }
                    //else
                    //{
                    //    code = StatusCodes.UniqueValidation;
                    //}
                }
            }
            //catch (DbUpdateConcurrencyException)
            //{
            //    code = StatusCodes.ConcurrancyError;
            //}
            catch (Exception ex)
            {
                code = StatusCodes.Error;
            }
            return code;
        }

        private bool TaskExists(int id)
        {
            return _dbConnection.Tasks.Count(e => e.TaskId == id && e.TaskId >0) > 0;
        }

        private bool IsTaskNameUnique(int id, string taskName)
        {
            bool isUnique = true;
            int noOfMatches = 0;
            noOfMatches = (from t in _dbConnection.Tasks where t.TaskId != id && t.TaskName == taskName select t).Count();
            isUnique = (noOfMatches == 0);

            return isUnique;
        }


    }
}
