﻿using System;
using System.Collections.Generic;
using TaskManager.DataLayer;
using TaskManager.Entities;



namespace TaskManager.BusinessLayer
{
    public class TaskBAL
    {
        
        private static TaskDAL _dalTask = new TaskDAL();

        public TaskEntity GetTaskByTaskId(int id)
        {
            return _dalTask.GetTaskById(id);
        }


        public List<TaskEntity> GetTasks()
        {
            return _dalTask.GetTasks().FindAll(x=>x.TaskId>0);
        }
 
        public StatusCodes SaveTask(TaskEntity task, int id=0)
        {
            StatusCodes code;

            if (task.TaskId == 0)
            {
               code= _dalTask.InsertTask(ref task);
            }
            else
            {
               code= _dalTask.UpdateTask(task);
            }
            
            return code;
        }
    }
}
