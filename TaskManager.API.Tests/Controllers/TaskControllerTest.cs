﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManager.API.Controllers;
using TaskManager.Entities;

namespace TaskManager.API.Tests.Controllers
{
    [TestClass]
    public class TaskControllerTest
    {

        #region  Get Test Methods

        [TestMethod]
        public void Get()
        {
            // Arrange

            TaskEntity t = new TaskEntity(); 
            TasksController controller = new TasksController();

            // Act
            IEnumerable<TaskEntity> result = controller.GetTasks();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Count()>0);
            Assert.AreNotEqual(1, result.ElementAt(0).TaskId);
        }

        [TestMethod]
        public void GetExistingTaskById()
        {
            // Arrange
           TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration(); 

            // Act
           HttpResponseMessage result = controller.GetTask(3);

            // Assert
            if (result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                TaskEntity t = result.Content.ReadAsAsync<TaskEntity>().Result;
                Assert.AreEqual(0, t.ParentTaskId);
                Assert.IsTrue(t.IsTaskEnded);
            }
        }

        [TestMethod]
        public void GetNonExistingTaskById()
        {
            // Arrange
            TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            
            // Act
            HttpResponseMessage result = controller.GetTask(100); // TaskId which does not exists.
            
            // Assert
            if (!result.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
            }
        }

        #endregion


        #region Post Test Methods

        [TestMethod]
        public void InsertTask()
        {
            // Arrange
            TasksController controller = new TasksController();

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            Random r = new Random(200);
           int id=  r.Next(201, 5000);
            TaskEntity entity = new TaskEntity();
            entity.TaskName = "Task " + id.ToString();
            entity.ParentTaskId = 2;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";
            

            // Act
           HttpResponseMessage response = controller.PostTask(entity);


            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            if (response.IsSuccessStatusCode)
            {
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.IsNotNull(message);   
            }
        }
        [TestMethod]
        public void UniqueValidationWhileInsert()
        {
            // Arrange
            TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TaskEntity entity = new TaskEntity();

            entity.TaskName = "A child Task";
            entity.ParentTaskId = 1;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";

            // Act
            HttpResponseMessage response = controller.PostTask(entity);

            // Assert
            if (response.IsSuccessStatusCode)
            {
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.IsNotNull(message);
            }
        }


        #endregion


        #region Put Test Methods

        [TestMethod]
        public void UpdateExistingTask()
        {
            // Arrange
            TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TaskEntity entity =  new TaskEntity();
            entity.TaskId = 1;
            entity.ParentTaskId = 0;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(5);
            entity.Status = "OPEN";
            entity.Priority = 26;
            entity.TaskName = "Task Modified using Test";

            // Act
            var response= controller.PutTask(1, entity);

            // Assert
            if(response.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(entity.ParentTaskId == 1);
                Assert.AreNotEqual("First Task", entity.TaskName);
            }

            
        }



        [TestMethod]
        public void UniqueValidationWhileUpdate()
        {
            // Arrange
            TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TaskEntity entity = new TaskEntity();
            entity.TaskId = 2;
            entity.TaskName = "A child Task";
            entity.ParentTaskId = 1;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";


            // Act
            HttpResponseMessage response = controller.PutTask(2,entity);


            // Assert
            
            if (!response.IsSuccessStatusCode)
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.AreEqual("Task Name should be unique.", message);
            }
        }



        [TestMethod]
        public void InValidInputWhileUpdate()
        {
            // Arrange
            TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TaskEntity entity = new TaskEntity();
            entity.TaskId = 2;
            entity.TaskName = "A child Task";
            entity.ParentTaskId = 1;
            entity.Priority = 25;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(15);
            entity.Status = "OPEN";


            // Act
            HttpResponseMessage response = controller.PutTask(1, entity);


            // Assert

            if (!response.IsSuccessStatusCode)
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
                string message = response.Content.ReadAsAsync<string>().Result;
                Assert.AreEqual("Invalid Task, Verify the Details.", message);
            }
        }


        [TestMethod]
        public void UpdateNonExistingTask()
        {
            // Arrange
            TasksController controller = new TasksController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            TaskEntity entity = new TaskEntity();
            entity.TaskId = 55;
            entity.ParentTaskId = 0;
            entity.StartDate = DateTime.Now;
            entity.EndDate = DateTime.Now.AddDays(5);
            entity.Status = "OPEN";
            entity.Priority = 26;
            entity.TaskName = "Task Modified using Test";

            // Act
            var response = controller.PutTask(55, entity);

            // Assert
            if (!response.IsSuccessStatusCode)
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(entity.ParentTaskId == 1);
                Assert.AreNotEqual("First Task", entity.TaskName);
            }
            else
            {
                Assert.AreNotEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }


        }

#endregion
        



    }
}
