﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using TaskManager.BusinessLayer;
using TaskManager.Entities;

namespace TaskManager.API.Controllers
{
    //[EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class TasksController : ApiController
    {
        private TaskBAL _taskBal = new TaskBAL();

        public List<TaskEntity> GetTasks()
        {
            return _taskBal.GetTasks();
        }



        // this is used toget the single record
           
        //GET: api/Tasks/5
        [ResponseType(typeof(TaskEntity))]
        public HttpResponseMessage GetTask(int id)
        {
            TaskEntity task = _taskBal.GetTaskByTaskId(id);
            if (task == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Task Details Not found");

            return Request.CreateResponse<TaskEntity>(HttpStatusCode.OK, task);
        }


        // PUT: api/Tasks/5
        [ResponseType(typeof(string))]
        public HttpResponseMessage PutTask(int id, [FromBody] TaskEntity task)
        {
           if (id != task.TaskId)
               return Request.CreateResponse<string>(HttpStatusCode.BadRequest,"Invalid Task, Verify the Details.");

            StatusCodes status=  _taskBal.SaveTask(task);


            if (status == StatusCodes.Success)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK,"Task Details are saved successfully");
            }
            else if (status == StatusCodes.ConcurrancyError)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Concurrency Issue, Please try again.");
            }
            else if (status == StatusCodes.UniqueValidation)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Task Name should be unique.");
            }
            else {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }
        }

        // POST: api/Tasks
        [ResponseType(typeof(string))]
        public HttpResponseMessage PostTask([FromBody] TaskEntity task)
        {
            

            StatusCodes status = _taskBal.SaveTask(task);

            if (status == StatusCodes.Success)
            {
                //return Request.CreateResponse<TaskEntity>(HttpStatusCode.OK, task);
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Task Details are saved successfully.");
            }
            else if (status == StatusCodes.ConcurrancyError)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Concurrency Issue, Please try again.");
            }
            else if (status == StatusCodes.UniqueValidation)
            {
                return Request.CreateResponse<string>(HttpStatusCode.OK, "Task Name should be unique.");
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, "Error Occured.");
            }
        }

        

       

      }
}